<?php

namespace Itul\PdfTools;

class PdfTools{

    private $_htmlWrapper;
    private $_autoSave                  = true;
    private $_withTableOfContents;
    private $_tableOfContentsOptions    = [];
    private $_withPageNumbers;
    private $_wrapperPdfRendered        = false;
    

    /**
     * Class method for initializing an instance of the class.
     *
     * @return static An instance of the class.
     */
    public static function init(){
        return new static;
    }

    /**
     * Retrieve the path to the temporary directory for PDF operations.
     *
     * @return string The path to the temporary directory.
     */
    public function _tempDir(){
        $dir = storage_path('pdf-tools');
        if(!file_exists($dir)) @mkdir($dir, 0755, true);
        return $dir;
    }

    /**
     * Generate a random name for output files.
     *
     * @param string $ext The file extension.
     * @param int|null $paging The paging format, if applicable.
     * @return string The randomly generated filename.
     */
    public function _randomName($ext = 'pdf', $paging = null){        
        $paging = !is_null($paging) ? $paging = '%'.$paging.'d' : '';
        return $this->_tempDir().'/'.md5(microtime(true).rand(0,999)).$paging.'.'.$ext;
    }

/* -----------------------------------------------------------------------------------------------------
* | PDF MODIFICATION METHODS
* ----------------------------------------------------------------------------------------------------- */

    /**
     * Split a PDF into individual pages.
     *
     * @param string $path The path to the PDF file.
     * @return array An array containing paths to the individual pages.
     */
    public function split($path){
        $path               = checkMimeType($path)->extension() != 'pdf' ? $this->toPdf($path) : $path;
        $pdf                = new \mikehaertl\pdftk\Pdf($path);
        $prefix             = uniqid('pdf_burst_');
        $temp_pdf_names     = $this->_tempDir().'/'.$prefix.'%04d.pdf';        
        $result             = $pdf->burst($temp_pdf_names);
        $res = glob(dirname($temp_pdf_names).'/'.$prefix.'*.pdf');
        foreach($res as $r) chmod($r, 0755);
        return $res;
    }

    /**
     * Merge multiple PDF files into a single PDF.
     *
     * @param array $paths An array of paths to PDF files to merge.
     * @return string The path to the merged PDF file.
     */
    public function merge(array $paths){

        //SET A TARGET OUTPUT PATH
        $target = $this->_randomName();

        //MERGE THE FILES
        (new \mikehaertl\pdftk\Pdf($this->toPdf($paths)))->cat()->saveAs($target);

        //SEND BACK THE TARGET
        return $target;
    }

    /**
     * Compress a PDF file.
     *
     * @param string|array $path The path to the PDF file or an array of paths.
     * @return string|array The path to the compressed PDF file or an array of paths.
     */
    public function compress($path){

        //CHECK FOR MULTIPLE PDFS
        if(is_array($path)){
            $ret = [];
            foreach($path as $p) $ret[] = $this->compress($p);
            return $ret;
        }

        //COMPRESS A SINGLE PDF (AND CONVERT TO PDF IF NEEDED)
        $target = $this->_randomName();
        (new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->compress(true)->saveAs($target);
        return $target;
    }

    /**
     * Uncompress PDF data.
     *
     * @param string|array $path The path to the compressed PDF file or an array of paths.
     * @return string|array The path to the uncompressed PDF file or an array of paths.
     */
    public function uncompress($path){

        //CHECK FOR MULTIPLE PATHS
        if(is_array($path)){
            $ret = [];
            foreach($path as $p) $ret[] = $this->uncompress($p);
            return $ret;
        }

        //UNCOMPRESS A SINGLE PDF
        $target = $this->_randomName();
        (new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->compress(false)->saveAs($target);
        return $target;
    }

    /**
     * Flatten a PDF to make text not selectable.
     *
     * @param string|array $path The path to the PDF file or an array of paths.
     * @return string|array The path to the flattened PDF file or an array of paths.
     */
    public function flatten($path){

        //CHECK FOR MULTIPLE PATHS
        if(is_array($path)){
            $ret = [];
            foreach($path as $p) $ret[] = $this->flatten($p);
            return $ret;
        }

        //FLATTEN A SINGLE PATH
        $target = $this->_randomName();
        (new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->flatten()->saveAs($target);
        return $target;
    }

    /**
     * Stamp a PDF with another PDF.
     *
     * @param string|array $path The path to the PDF file or an array of paths.
     * @param string $stampPath The path to the PDF file to use as a stamp.
     * @param bool $allPages Whether to stamp all pages of the PDF.
     * @return string|array The path to the stamped PDF file or an array of paths.
     */
    public function stamp($path, string $stampPath, $allPages = false){

        //FORCE THE STAMP TO BE A PDF
        $stampPath = $this->toPdf($stampPath);

        if(is_array($path)){
            $ret = [];
            foreach($path as $p) $ret[] = $this->stamp($p, $stampPath, $allPages);
            return $ret;
        }
        
        //CHECK IF WE WANT TO STAMP ALL OF THE PAGES
        //if($allPages){
        //    $pages = [];
        //    foreach($this->split($path) as $page) $pages[] = $this->stamp($page, $stampPath);
        //    return $this->merge($pages);
        //}
        
        //STAMP THE FILE
        $target = $this->_randomName();
        (new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->stamp($stampPath)->saveAs($target);        
        return $target;
    }

    /**
     * Fill PDF form fields with data.
     *
     * @param string $path The path to the PDF file.
     * @param array $data An associative array containing form field data.
     * @return string The path to the filled PDF file.
     */
    public function fill(string $path, array $data = []){
        $target = $this->_randomName();
        (new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->fillForm($data)->saveAs($target);
        return $target;
    }

/* -----------------------------------------------------------------------------------------------------
* | DATA METHODS
* ----------------------------------------------------------------------------------------------------- */

    /**
     * Get PDF information.
     *
     * @param string $path The path to the PDF file.
     * @return \GeneralObject An object containing the PDF information.
     */
    public function pdfInfo(string $path){
        return new \GeneralObject(json_decode(json_encode((new \mikehaertl\pdftk\Pdf($this->toPdf($path)))->getData()->__toArray())));
    }

    /**
     * Get the number of pages in a PDF.
     *
     * @param string $path The path to the PDF file.
     * @return float The number of pages in the PDF.
     */
    public function pageCount(string $path){
        return floatval($this->pdfInfo($path)->NumberOfPages);
    }    

/* -----------------------------------------------------------------------------------------------------
* | FORMAT CONVERSION METHODS
* ----------------------------------------------------------------------------------------------------- */

    /**
     * Convert a file to a PDF.
     *
     * @param string|array $path The path to the file or an array of file paths.
     * @return string|array The path to the converted PDF or an array of paths if multiple files were converted.
     */
    public function toPdf($path){

        //PREVENT CONVERTING PDFS AGAIN
        if(is_string($path) && checkMimeType($path)->extension() == 'pdf') return $path;

        //HANDLE MULTI PATH CONVERSION
        if(is_array($path)){
            $ret = [];
            foreach($path as $p) $ret[] = $this->toPdf($p);
            return $ret;
        }

        //GET A UNIQUE OUTPUT NAME
        $target = $this->_randomName();

        //CONVERT TO PDF
        exec("convert {$path} -quality 100 {$target}");

        //SEND BACK THE TARGET
        return $target;
    }

    /**
     * Convert a file to an image.
     *
     * @param string $path The path to the file.
     * @param string $format The image format to convert to (default is 'png').
     * @param int $density The density for the image conversion (default is 300).
     * @return array The paths to the converted images.
     */
    public function toImage(string $path, $format = 'png', $density = 300){

        //DONT CONVERT IMAGES THAT ALREADY MATCH THE OUTPUT FORMAT
        if(checkMimeType($path)->extension() == strtolower($format)) return $path;

        //DEFINE A FILE PATH PREFIX
        $img_prefix         = uniqid('pdf_to_image_', true);

        //DEFINE A TARGET OUTPUT DIRECTORY AND NAMING FORMAT
        $temp_img_names     = $this->_tempDir().'/'.$img_prefix.'%04d.'.$format;

        //RUN THE CONVERSION
        exec("convert -colorspace RGB -interlace none -density {$density}x{$density} {$path} -quality 100 {$temp_img_names}");

        //SEND BACK THE FILES THAT MATCH THE FORMAT
        return              glob(dirname($temp_img_names).'/'.$img_prefix.'*.'.$format);
    }

/* -----------------------------------------------------------------------------------------------------
* | HTML RENDERING METHODS
* ----------------------------------------------------------------------------------------------------- */

    /**
     * Get the wrapper for HTML to PDF conversion.
     *
     * @return \Barryvdh\DomPDF\PDF The DomPDF wrapper.
     */
    private function _getHtmlWrapper(){
        if(is_null($this->_htmlWrapper)){
            //$wrapper = \Barryvdh\DomPDF\PDF::setPaper('letter');
            $wrapper = \App::make('dompdf.wrapper');
            $wrapper->setOption(['isRemoteEnabled' => true, 'isPhpEnabled' => true])->getDomPDF()
                ->setPaper('letter')
                
                ->setHttpContext(stream_context_create(['ssl' => ['allow_self_signed' => TRUE, 'verify_peer' => FALSE, 'verify_peer_name' => FALSE]]));
            $this->_htmlWrapper = $wrapper;
        }

        return $this->_htmlWrapper;
    }

    /**
     * Toggle the value for auto saving a file.
     *
     * @param boolean $val The true/false toggle.
     * @return \Itul\PdfTools\PdfTools The PdfTools class.
     */
    public function autoSave($val = true){

        //SET THE AUTO SAVE TOGGLE
        $this->_autoSave = $val;

        //RETURN THE CLASS
        return $this;
    }

    /**
     * Enable/Disable a Table of contents.
     *
     * @param callable|boolean $callback A callback function table of contents rendering or a boolean value to use the default rendering.
     * @return \Itul\PdfTools\PdfTools The PdfTools class.
     */
    public function withTableOfContents($callback = true, array $options = []){

        $this->_withTableOfContents = $callback;
        $this->_tableOfContentsOptions = $options;
        return $this;
    }

    /**
     * Enable/Disable page numbers.
     *
     * @param callable|boolean $callback A callback function table of contents rendering or a boolean value to use the default rendering.
     * @return \Itul\PdfTools\PdfTools The PdfTools class.
     */
    public function withPageNumbers($callback = true){
        $this->_withPageNumbers = $callback;
        return $this;
    }

    private function _generateTableOfContents(string $html){

        if(is_callable($this->_withTableOfContents) || $this->_withTableOfContents === true){

            //PARSE THE HTML
            $htmlDoc = (new \simplehtmldom\HtmlDocument)->load($html);

            //DEFINE DEFAULTS
            $mainChapters   = [];
            $chapters       = [];
            $k              = -1;

            //LOOP THROUGH THE PAGE SECTIONS
            foreach($htmlDoc->find('body > section.page') as $pageKey => $pageSection){

                $k++;

                //LOOP THROUGH THE HEADERS
                foreach($pageSection->find('h1,h2,h3,h4,h5,h6') as $ele){                     

                    //INCREMENT THE COUNTER
                    $k++;

                    $toc_ignore = (@$ele->attr['toc-ignore'] == 'true') ? true : false;

                  
                    //DEFINE THE CHAPTER AS A DYNAMIC CLASS
                    $chapter = new class([
                        'name'                  => ($toc_ignore) ? 'ignored' :  $ele->plaintext,
                        'node'                  => $ele,
                        'indent'                => (integer)str_replace('h', '', $ele->tag),
                        'children'              => collect([]),
                        'pageKey'               => $pageKey,
                        'ident'                 => str_pad($k, 4, '0', STR_PAD_LEFT), 
                        'toc_ignore'            => $toc_ignore,
                        'render_row_callback'   => ['callbackFunc' => $this->_withTableOfContents],
                    ]) extends \GeneralObject {

                        //RENDER A SPECIFIC ROW ELEMENT
                        public function renderRowElement($obj){

                            //OPEN THE TAG
                            $str = '<'.$obj->tag.' ';

                            //LOOP THROUGH THE ATTRIBUTES
                            foreach($obj->attr as $attrName => $attrVal){

                                //BUILD INLINE STYLES
                                if($attrName == 'style'){
                                    $str .= ' style="';
                                    foreach($attrVal as $styleKey => $styleVal) $str .= $styleKey.':'.$styleVal.';';
                                    $str .= '" ';
                                }

                                //BUILD CSS CLASS DEFINITIONS
                                elseif($attrName == 'class') $str .= ' class="'.(is_array($attrVal) ? implode(' ', $attrVal) : $attrVal ).'"';

                                //APPEND ANY OTHER ATTRIBUTES
                                else $str .= ' '.$attrName.'="'.(is_string($attrVal) ? $attrVal : addslashes(json_encode($attrVal))).'"';
                            }

                            //END THE OPEN TAG
                            $str .= '>';

                            //LOOP THROUGH THE CHILDREN FOR THIS ELEMENT AND RENDER THEM IF NEEDED
                            foreach($obj->children as $child) $str .= "\n".(string)$child;

                            //CLOSE THE TAG
                            $str .= '</'.$obj->tag.'>';

                            //RETURN THE RENDERED STRING
                            return $str;
                        }

                        //RENDER NESTED TABLE OF CONTENTS ROWS
                        public function renderRow(){

                            //INJECT AN ANCHOR LINK IN THE HEADING FOR THE TABLE OF CONTENT
                            $this->node->innertext .= '<a name="_anchored_header'.$this->ident.'">&nbsp;</a>';

                            //INJECT THE PAGE NUMBER LOGIC
                            $this->node->outertext = "\n".$this->node->outertext."\n".'<script type="text/php"> $GLOBALS[\'_toc_chapters\'][\''.$this->ident.'\'] = $pdf->get_page_number(); </script>'."\n";

                            //DEFAULT THE STRING OUTPUT TO BE EMPTY
                            $str = '';

                            //BUILD THE ROW
                            if($this->name != '' && $this->name != 'ignored'){
                                
                                //DEFINE THE ELEMENT DATAS
                                $multipler              = 2;
                                $this->display_indent   = $this->indent;
                                $chapterIdentifier      = '%%CH'.$this->ident.'%%';
                                $indentSpaces           = str_repeat("&nbsp;", ($this->display_indent-1)*$multipler);
                                $anchorId               = '#_anchored_header'.$this->ident;                                

                                //BUILD THE ANCHOR LINK ELEMENT
                                $tdElement1ATag = new class(['tag' => 'a', 'children' => collect([$this->name]), 'attr' => ['href' => $anchorId, 'style' => ['font-size' => '13px', 'color' => '#333', 'text-decoration' => 'none'], 'class' => []], 'chapter' => $this]) extends \GeneralObject {
                                    public function __toString(){
                                        return $this->chapter->renderRowElement($this);
                                    }
                                };

                                //BUILD THE TITLE CELL FOR THE ROW
                                $tdElement1 = new class(['tag' => 'td', 'children' => collect([$indentSpaces, $tdElement1ATag]), 'attr' => ['style' => ['font-size' => '13px', 'border-bottom' => '1px dashed #ccc !important', 'padding-left' => '0px'], 'class' => []], 'chapter' => $this]) extends \GeneralObject {
                                    public function __toString(){
                                        return $this->chapter->renderRowElement($this);
                                    }
                                };

                                //BUILD THE PAGE NUMBER FOR THE ROW
                                $tdElement2 = new class(['tag' => 'td', 'children' => collect([$chapterIdentifier]), 'attr' => ['style' => ['font-size' => '13px', 'border-bottom' => '1px dashed #ccc !important', 'text-align' => 'right !important', 'padding-right' => '0px'], 'align' => 'right', 'class' => []], 'chapter' => $this]) extends \GeneralObject {
                                    public function __toString(){
                                        return $this->chapter->renderRowElement($this);
                                    }
                                };

                                //px($tdElement2->__toString());

                                //BUILD THE ROW
                                $trElement = new class(['tag' => 'tr', 'children' => collect([$tdElement1,$tdElement2]), 'attr' => ['style' => [], 'class' => []], 'chapter' => $this]) extends \GeneralObject {
                                    public function __toString(){
                                        return $this->chapter->renderRowElement($this);
                                    }
                                };
                                
                                //CHECK FOR RENDERING CALLBACK (ALLOWS A DEVELOPER TO CUSTOMIZE THE ROW STYLES)
                                if(is_callable($this->render_row_callback['callbackFunc'])) $this->render_row_callback['callbackFunc']($this, $trElement);

                                //GET THE ROW AS A HTML STRING
                                $str = (string)$trElement;
                            }

                            //BUILD CHILD ROWS
                            if($this->children->count()) foreach($this->children as $child) $str .= $child->renderRow();
                           
                            //SEND BACK THE RENDERED HTML
                            return $str;
                        }
                    };

                
                    //ADD THE CHAPTER TO THE COLLECTION
                    $chapters[$k] = $chapter;

                    //CHECK FOR TOP LEVEL CHAPTER
                    if($chapter->indent == 2){
                        $mainChapters[] = $chapter;
                    }
                    //NOT A TOP LEVEL CHAPTER
                    else{

                        //CHECK IF THE PREVIOUS CHAPTER EXISTS
                        if(isset($chapters[($k-1)])){

                            //CHECK IF THE PREVIOUS CHAPTER BELONGS TO THE SAME PAGE AND SHOULD BE A CHILD
                            if($chapters[($k-1)]->pageKey == $pageKey && $chapters[($k-1)]->indent < $chapter->indent){

                                //ADD THE CHAPTER AS A CHILD
                                $chapters[($k-1)]->children[] = $chapter;
                            }

                            //TRY TO FIND THE CLOSEST PARENT
                            else{

                                $k2 = ($k-2);
                                while($k2 > -1){
                                    if(isset($chapters[$k2]) && $chapters[($k2)]->pageKey == $pageKey && $chapters[$k2]->indent < $chapter->indent){
                                        $chapters[$k2]->children[] = $chapter;
                                        break;
                                    }
                                    $k2 = $k2-1;
                                }
                            }
                        }
                    }
                }
            }

            // RESET INDNET FOR IGNORED CHAPTERS
            foreach($mainChapters as $key => $chapter) if($chapter->toc_ignore == true) $chapter->indent = @$mainChapters[$key-1]->indent;  

            $bodyPrepend    = '';
            $bodyAppend     = ''; 

            //INJECT DEFAULTS FOR PAGE TRACKING  
            $bodyPrepend = "\n".'<script type="text/php">
                $GLOBALS[\'_toc_chapters\'] = [];
                $GLOBALS[\'_toc_max_object\'] = 0;
            </script>'."\n";

            //GENERATE THE TABLE OF CONTENTS
            $bodyPrepend .= "<section class=\"page\"><h1 style=\"margin-bottom:20px; text-align:center;\">Table of Contents</h1><table class=\"table\" style=\"border:none; padding:0px !important; margin:0px !important;\" cellspacing=\"0\" cellpadding=\"0\"><tbody>";
            foreach($mainChapters as $chapter) $bodyPrepend .= $chapter->renderRow();
            $bodyPrepend .= "</tbody></table></section>";

            //CALCULATE THE PDF OBJECTS
            $bodyPrepend .= "\n\n".'<script type="text/php">
                $GLOBALS[\'_toc_max_object\'] = count($pdf->get_cpdf()->objects);
                $GLOBALS[\'_first_non_toc_page\'] = 0;
            </script>'."\n\n";

            //GENERATE THE PAGE CALCULATION LOGIC
            $bodyAppend = "\n\n".'<script type="text/php">

                //LOOP UNTIL ALL CHAPTERS HAVE BEEN ACCOUNTED FOR
                for($i = 0; $i <= $GLOBALS[\'_toc_max_object\']; $i++){

                    //VALIDATE THE AN ELEMENT OF THE PDF OBJECT EXISTS
                    if(!array_key_exists($i, $pdf->get_cpdf()->objects)) continue;

                    //DEFINE THE TABLE OF CONTENTS OBJECT
                    $_toc_object = $pdf->get_cpdf()->objects[$i];

                    //LOOP THROUGH ALL OF THE CHAPTERS
                    foreach ($GLOBALS[\'_toc_chapters\'] as $_toc_chapter => $_toc_page){

                        //CHECK IF THE CHAPTER IS PART OF THE TABLE OF CONTENTS OBJECT
                        if(array_key_exists(\'c\', $_toc_object)){

                            if($GLOBALS[\'_first_non_toc_page\'] === 0) $GLOBALS[\'_first_non_toc_page\'] = $_toc_page;

                            //REPLACE THE PLACEHOLDER PAGE NUMBER WITH THE ACTUAL PAGE NUMBER
                            $_toc_object[\'c\'] = str_replace(\'%%CH\'.$_toc_chapter.\'%%\', str_repeat(\' \', 16).$_toc_page, $_toc_object[\'c\'] );

                            //SET THE TABLE OF CONTENTS OBJECT
                            $pdf->get_cpdf()->objects[$i] = $_toc_object;
                        }
                    }
                }
            </script>'."\n\n";           

            //GENERATE COMPILED HTML
            $body               = $htmlDoc->find('body', 0);
            $innertext          = (string)$body->innertext;
            $innertext          = $bodyPrepend;
            $body->innertext    = $innertext;
            $compiledHtml       = str_replace('</body></html>', $bodyAppend.'</body></html>', $htmlDoc->save());

            //GENERATE THE PDF DATA
            $wrapper            = $this->_getHtmlWrapper();
            $pdf                = $wrapper->getDomPDF();
            $wrapper->loadHtml($compiledHtml);
            $pdf->render();
            $this->_wrapperPdfRendered = true;
        }
        
        return $this;
    }

    private function _generatePageNumbers(string $html){
        if(is_callable($this->_withPageNumbers) || $this->_withPageNumbers === true){
            $wrapper = $this->_getHtmlWrapper();
            if(!$this->_wrapperPdfRendered) $wrapper->loadHtml($html);
            $pdf = $wrapper->getDomPDF();

            $callback = ['callbackFunc' => $this->_withPageNumbers];
            $hasTOC = is_callable($this->_withTableOfContents) || $this->_withTableOfContents === true;

            //GENERATE PAGE NUMBERS
            $pdf->getCanvas()->page_script(function($pageNumber, $pageCount, $canvas, $fontMetrics) use($callback, $hasTOC){

                if($hasTOC && $pageNumber >= $GLOBALS['_first_non_toc_page']){
                    $text       = "Page ".($pageNumber)." of ".($pageCount);
                    $font       = $fontMetrics->getFont('helvetica', 'italic');
                    $pageWidth  = $canvas->get_width();
                    $pageHeight = $canvas->get_height();
                    $size       = 10;
                    $width      = $fontMetrics->getTextWidth($text, $font, $size);
                    $left       = $pageWidth - $width - 50;
                    $top        = $pageHeight - 40;
                    $color      = [0,0,0,0.5];

                    if(is_callable($callback['callbackFunc'])){
                        $obj = new class([
                            'pageNumber'    => $pageNumber,
                            'pageCount'     => $pageCount,
                            'canvas'        => $canvas,
                            'fontMetrics'   => $fontMetrics,
                            'text'          => $text,
                            'font'          => $font,
                            'left'          => $pageWidth - $width - 50,
                            'top'           => $pageHeight - 40,
                            'pageWidth'     => $pageWidth,
                            'pageHeight'    => $pageHeight,
                            'size'          => $size,
                            'width'         => $width,
                            'color'         => $color,
                        ]) extends \GeneralObject {};

                        $callback['callbackFunc']($obj);
                        $canvas->text($obj->left, $obj->top, $obj->text, $obj->font, $obj->size, $obj->color);
                    }
                    else{
                        $canvas->text($left, $top, $text, $font, $size, $color);
                    }
                }
                
            });
            $this->_wrapperPdfRendered = true;
        }
        
        return $this;
    }

    /**
     * Convert an HTML file or a string of HTML to a PDF.
     *
     * @param string $html The HTML content or file path.
     * @param array $options Options for how the html is rendered.
     * @return string The path to the generated PDF.
     */
    public function fromHTML(string $html, array $options = []){

        $transparent = @$options['transparent'] == true;

        //CHECK IF THE HTML IS A FILE
        $html = @file_exists($html) ? file_get_contents($html) : $html;

        //DEFINE THE DEFAULT STYLES
        $style = '
            @page { /*size: A4 portrait;*/ margin:0cm; }
            body { color: #58585a; background-color: '.($transparent ? 'transparent' : '#ffffff').'; font-size: 13px; padding:.5cm; }
            .keep-together { page-break-inside: avoid; }
            section.page { page-break-after: always; padding:1cm; margin:0; page-break-inside: auto;}
            section.vcenter { height:100%; display:block; position:relative; }
            .vcenter .content-block{ position:absolute; top:50%; height:50%; margin-top:-25%; padding-right:1.5cm; width:87%; }
            li { page-break-before: auto; page-break-inside: avoid; padding-bottom:10px; }
            p { page-break-inside: avoid; }
            li > p { padding:0px; margin:0px; }
            section.page:last-of-type { page-break-after: unset; }
            section.page.no-break { page-break-after: unset; }
            .table { width: 100%; border-collapse: collapse; border-spacing: 0px; border: 1px solid lightgray; }
            .table > thead th { border-bottom:1px solid #ccc; background: #d9d9d9; font-weight: bold; padding: 5px; font-size:13px;}
            .table > tbody > tr > td { border-bottom:1px solid #ccc; padding:5px; font-size: 14px; }
            .table > tbody > tr > td.col-12, .table > thead > tr > th.col-12 { width:100%; }
            .table > tbody > tr > td.col-11, .table > thead > tr > th.col-11 { width:91.66%; }
            .table > tbody > tr > td.col-10, .table > thead > tr > th.col-10 { width:83.33%; }
            .table > tbody > tr > td.col-9, .table > thead > tr > th.col-9 { width:75%; }
            .table > tbody > tr > td.col-8, .table > thead > tr > th.col-8 { width:66.66%; }
            .table > tbody > tr > td.col-7, .table > thead > tr > th.col-7 { width:58.33%; }
            .table > tbody > tr > td.col-6, .table > thead > tr > th.col-6 { width:50%; }
            .table > tbody > tr > td.col-5, .table > thead > tr > th.col-5 { width:41.66%; }
            .table > tbody > tr > td.col-4, .table > thead > tr > th.col-4 { width:33.33%; }
            .table > tbody > tr > td.col-3, .table > thead > tr > th.col-3 { width:25%; }
            .table > tbody > tr > td.col-2, .table > thead > tr > th.col-2 { width:16.66%;}
            .table > tbody > tr > td.col-1, .table > thead > tr > th.col-1 { width:8.33%;}
            .table.table-striped > tbody > tr.odd > td { background-color: #f4f4f4; }
            section.page > .page-body { padding:0px; font-size: 13px; line-height: 1.2; }
        ';

        //TRY TO APPEND THE DEFAULT STYLES
        $dom = new \DOMDocument;
        @$dom->loadHTML($html);
        $head = $dom->getElementsByTagName('head')->item(0);
        $body = false;
        if(is_null($head)) if($head = $dom->getElementsByTagName('body')->item(0)) $body = $head;
        if(is_null($head)) $html = '<style>'.$style.'</style>'.$html;
        else{
            if($body) $body->prepend($dom->createElement('style', $style));
            else $head->prepend($dom->createElement('style', $style));
            $html = $dom->saveHtml();
        }

        //DEFINEE THE DEFAULT OPTIONS
        $defaultOptions = [
            'paper'         => 'letter',
            'orientation'   => 'portrait',
        ];

        //BUILD THE OPTIONS ARRAY
        if(!is_array($options) || is_array($options) && empty($options)) $options = [];
        foreach($defaultOptions as $optionKey => $optionVal) if(!array_key_exists($optionKey, $options)) $options[$optionKey] = $optionVal;

        //START THE WRAPPER
        $this->_getHtmlWrapper()->set_paper($options['paper'], $options['orientation']);

        //GENERATE TABLE OF CONTENTS AND PAGE NUMBERS IF NEEDED
        $this->_generateTableOfContents($html);
        $this->_generatePageNumbers($html);

        $wrapper = !$this->_wrapperPdfRendered ? $this->_getHtmlWrapper()->loadHTML($html) : $this->_getHtmlWrapper();

        //CHECK IF AUTOSAVE IS DISABLED
        if($this->_autoSave === false) return $this;

        //AUTOSAVE ENABLED SO SAVE THE FILE TO A TEMP DIRECTORY
        $target     = $this->_randomName();
        $wrapper->save($target);

        //RETURN THE TEMP FILE
        return $target;
    }

    /**
     * Render a Laravel view as a PDF.
     *
     * @param string $view The name of the view.
     * @param array $data The data to pass to the view.
     * @return string The path to the generated PDF.
     */
    public function fromView(string $view, array $data = [], array $options = []){

        //SEND THE RENDERED VIEW TO THE fromHTML METHOD
        return $this->fromHtml(view($view, $data)->render(), $options);
    }
    
    /**
     * Convert a fillable PDF to an HTML form.
     *
     * @param string $path The path to the fillable PDF.
     * @param bool $raster Whether to rasterize the PDF.
     * @param bool $gen Whether to generate the parsed HTML.
     * @return mixed The generated HTML or an object containing the parsed data.
     */
    public function toHTML($path, $raster = false, $gen = true){

        $res = new class($path, $raster) extends \GeneralObject {

            public function __construct($path, bool $raster = false){
                parent::__construct([
                    'originalPath'  => $path, 
                    'raster'        => $raster, 
                    'fillValues'    => [], 
                    'styles'        => "",
                ]);
            }

            public function generate(){

                ini_set('memory_limit', '-1');
                copy($this->originalPath, ($tmpCopy = sys_get_temp_dir().'/'.uniqid().'.pdf'));

                //SELECTABLE TEXT
                $res        = ['style' => '','pages' => []];
                foreach((new \Itul\PdfTools\PdfTools)->split($tmpCopy) as $tmpPageKey => $tmpPage){

                    $helper         = \Itul\PdfData\PdfData::of($tmpPage)->rasterize($this->raster);
                    $res['style']   = $helper->_defaultPageStyles;
                    $pages          = $helper->pages();

                    if($pages->count()) foreach($pages as $page) $res['pages'][] = $page->render(null, (isset($this->fillValues[$tmpPageKey]) && is_array($this->fillValues[$tmpPageKey]) ? $this->fillValues[$tmpPageKey] : []), true, false);
                }

                $this->parsed = $res;

                return $this;
            }

            public function fill(array $data = []){
                $this->fillValues = $data;
                return $this;
            }

            public function addStyle(string $content){
                $this->styles = '<style>'.(str_replace(['<style>', '</style>'], ['',''], $this->styles."\n".$content)).'</style>'."\n";
                return $this;
            }
        };

        return $gen ? $res->generate()->parsed : $res;
    }

    /**
     * Convert a single HTML page to a fillable PDF form.
     *
     * @param string $pageHtml The HTML content or file path.
     * @param string|null $pageStyle The CSS style content or file path.
     * @return string The path to the generated PDF.
     */
    public function fillableFromHtml(string $pageHtml, string $pageStyle = null){

        //CHECK IF PAGEHTML IS A FILEPATH
        if(file_exists($pageHtml)) $pageHtml = @file_get_contents($pageHtml);

        //SET THE PAGE STYLE
        $pageStyle = is_null($pageStyle) ? '' : $pageStyle;

        //CHECK IF THE PAGE STYLE IS A STRING
        if(is_string($pageStyle)){

            //CHECK IF THE PAGE STYLE HAS ANY DATA
            if(!empty($pageStyle)){

                //HANDLE EXTERNAL STYLESHEETS
                if(file_exists($pageStyle)) $pageStyle = @file_get_contents($pageStyle);
                elseif($tmpPageStyle = @file_get_contents($pageStyle)) $pageStyle = $tmpPageStyle;

                //PARSE STYLESHEET
                if(strpos($pageStyle, '<style>') !== false) $pageStyle = str_replace(['<style>','</style>'], ['',''], $pageStyle);
            }
        }

        //FORCE FIELDS TO USE DISTINCT NAMES
        if(preg_match_all("/<(?:(input|textarea)) (?:.*?)name=\"(.*?)#(.*?)\"/", $pageHtml, $pageHtmlMatches)) if(is_array($pageHtmlMatches[0]) && count($pageHtmlMatches[0])) foreach($pageHtmlMatches[0] as $pageHtmlMatchKey => $pageHtmlMatch) $pageHtml = str_replace($pageHtmlMatch, str_replace($pageHtmlMatches[2][$pageHtmlMatchKey].'#'.$pageHtmlMatches[3][$pageHtmlMatchKey], $pageHtmlMatches[2][$pageHtmlMatchKey].'_'.$pageHtmlMatches[3][$pageHtmlMatchKey], $pageHtmlMatch), $pageHtml);

        //PAGE DEFAULTS
        $pageWidth      = 612;
        $pageHeight     = 792;
        $imageWidth     = $pageWidth.'pt';
        $imageHeight    = $pageHeight.'pt';
        $pageParts      = explode("\n", $pageHtml);

        if(is_array($pageParts) && !empty(array_filter($pageParts))){
            $pageParts = array_filter($pageParts);
            array_shift($pageParts);
            array_pop($pageParts);
            
            if(preg_match('/width="(.*?)"/', $pageParts[0], $imageMatchesWidth)){
                $pageWidth  = round(floatval($imageMatchesWidth[1])/1.3333333334);
                $imageWidth = $pageWidth.'pt';
                $pageParts[0] = str_replace($imageMatchesWidth[0], 'width="'.$imageWidth.'"', $pageParts[0]);
            }
            if(preg_match('/height="(.*?)"/', $pageParts[0], $imageMatchesHeight)){
                $pageHeight     = round(floatval($imageMatchesHeight[1])/1.3333333334);
                $imageHeight    = $pageHeight.'pt';
                $pageParts[0] = str_replace($imageMatchesWidth[0], 'height="'.$imageHeight.'"', $pageParts[0]);
            }
            
            $pageParts[0]       = '<div style="position:absolute; top:0pt; left:0pt; height:'.$imageHeight.'; width:'.$imageWidth.'">'.$pageParts[0].'</div>';
            $pageHtml           = implode("\n", $pageParts);
        }

        $mpdf                   = new \Mpdf\Mpdf();
        $mpdf->useActiveForms   = true;
        //$mpdf->bleedMargin = 0;

        $tmpPageHtml = "<html lang=\"en\">
<head>
<style>
{$pageStyle}
</style>
<style>
    @page {
        margin:0;
        size:           
    }
    textarea {
        margin:0pt;
        padding:0pt;
    }

    input[type=text] {
        margin:0pt;
        padding:0pt;
    }
</style>
</head>
<body>
{$pageHtml}
</body>
</html>";           

        //FIX BACKGROUND IMAGES
        if(preg_match("/(background-image: url\(\"data:image(.*?)\"\);(.*?))/", $tmpPageHtml, $svgImageMatches)) $tmpPageHtml = str_replace($svgImageMatches[0], '', $tmpPageHtml);

        //APPLY THE INLINE STYLE SHEET
        $dom                    = new \InlineStyle\InlineStyle($tmpPageHtml);
        $dom->applyStylesheet($dom->extractStylesheets());
        $tmpPageHtml            = $dom->getHTML();

        //FIX FRAGMENTED HTML
        $dom                    = new \DOMDocument();
        $dom->loadHTML($tmpPageHtml);
        $tmpPageHtml            = $dom->saveHtml();

        //FIND INPUTS
        preg_match_all("/<(?:(input|textarea))[^>]+ (?:style=\"(.*?)\")>(?:((.*?)<\/textarea>)?)/im", $tmpPageHtml, $styleMatches);

        //LOOP THROUGH THE INPUTS
        foreach($styleMatches[0] as $styleMatchKey => $styleMatch){

            //SET INPUT STYLE MATCHING DEFAULTS
            $newStyleMatch      = $styleMatch;
            $parsedStyle        = [];
            $inlineParsedStyle  = [];

            //PARSE INLINE STYLE POSITIONS
            if(preg_match_all("/([\w-]+)\s*:\s*([^;]+)\s*;?/", $styleMatches[2][$styleMatchKey], $styleStringMatches, PREG_SET_ORDER)) foreach ($styleStringMatches as $styleStringMatch) if(in_array(strtolower(trim($styleStringMatch[1])), ['position','top','left','right','bottom','width','height'])) $parsedStyle[$styleStringMatch[1]] = $styleStringMatch[2];
            
            //PARSE ALL INLINE STYLES
            if(preg_match_all("/([\w-]+)\s*:\s*([^;]+)\s*;?/", $styleMatches[2][$styleMatchKey], $styleStringMatches, PREG_SET_ORDER)) foreach ($styleStringMatches as $styleStringMatch) $inlineParsedStyle[$styleStringMatch[1]] = $styleStringMatch[2];

            //CHECK IF THERE ARE PARSED STYLES
            if(!empty($parsedStyle)){

                //PARSE THE NEW STYLE
                $newStyle = [];
                foreach($parsedStyle as $parsedStyleKey => $parsedStyleVal) $newStyle[] = $parsedStyleKey.":".$parsedStyleVal;
                $newStyle   = implode("; ", $newStyle);

                //FIX EMPTY TEXTAREA INPUTS
                if(trim(strtolower($styleMatches[1][$styleMatchKey])) == 'textarea') $newStyleMatch = str_replace("></textarea", ">&nbsp;</textarea", $newStyleMatch);

                //UPDATE THE HTML WITH THE PARSED STYLES
                $tmpPageHtml = str_replace($styleMatch, '<div style="'.$newStyle.'">'.$newStyleMatch.'</div>', $tmpPageHtml);
            }
        }

        //CHECK IF THERE ARE CUSTOM CHECKBOXES TO PARSE
        if(preg_match_all('/(<label.+?class=".*?custom-checkbox.*?">)(.*?)<\/label>/', $tmpPageHtml, $customCheckboxMatches)){

            //LOOP THROUGH THE CUSTOM CHECKBOX MATCHES
            foreach($customCheckboxMatches[0] as $customCheckboxMatchKey => $customCheckBoxMatch){

                //SETUP CUSTOM CHECKBOX DEFAULTS
                $extractStyleString = $customCheckboxMatches[1][$customCheckboxMatchKey];
                $input              = str_replace('<span class="checkmark"></span>', '', $customCheckboxMatches[2][$customCheckboxMatchKey]);
                $replacement        = '';

                //CHECK IF THERE ARE STYLES TO FIX FOR CUSTOM CHECKBOXES
                if(preg_match_all("/([\w-]+)\s*:\s*([^;\">]+)\s*;?/", $extractStyleString, $styleStringMatches, PREG_SET_ORDER)){

                    //SETUP PARSING DEFAULTS
                    $inlineParsedStyle  = ['position' => 'absolute', 'margin' => '0pt', 'padding' => '0pt','border:0pt'];
                    $newInlineStyle     = [];

                    //BUILD PARSING ARRAYS
                    foreach ($styleStringMatches as $styleStringMatch) $inlineParsedStyle[$styleStringMatch[1]] = $styleStringMatch[2];
                    if(isset($inlineParsedStyle['top'])) $inlineParsedStyle['top']      = (floatval(str_replace('pt', '', $inlineParsedStyle['top']))-1.33334).'pt';
                    if(isset($inlineParsedStyle['left'])) $inlineParsedStyle['left']    = (floatval(str_replace('pt', '', $inlineParsedStyle['left']))-1.33334).'pt';
                    $inlineParsedStyle['border']        = '0pt';
                    $inlineParsedStyle['display']       = 'block';
                    $inlineParsedStyle['background']    = 'transparent';

                    foreach($inlineParsedStyle as $iPK => $iPV) $newInlineStyle[] = $iPK.':'.$iPV;
                    $checkboxInlineStyle = $newInlineStyle;
                    $input = str_replace('type="checkbox"', 'type="checkbox" style="'.implode('; ', $newInlineStyle).'"', $input);

                    //DEFINE THE NEW REPLACEMENT
                    $replacement        = '<div style="'.implode("; ", $newInlineStyle).'">'.$input.'</div>';
                }

                //REPLACE HTML FRAGMENTS WITH THE PARSED REPLACEMENT
                $tmpPageHtml = str_replace($customCheckboxMatches[0][$customCheckboxMatchKey], $replacement, $tmpPageHtml);
            }
        }

        //CLEAN UP CHARACTER ENCODING
        $tmpPageHtml = str_replace('&acirc;&#128;&#153;', "'", $tmpPageHtml);
        $tmpPageHtml = str_replace('&Acirc;', '', $tmpPageHtml);

        //TELL mPDF TO WRITE THE HTML
        $mpdf->WriteHTML($tmpPageHtml);

        //SET THE OUTFILE
        $outFile = sys_get_temp_dir().'/'.md5(microtime(true).rand(0,999)).'.pdf';

        //SAVE THE PDF TO THE OUTFILE
        $mpdf->Output($outFile, false);

        return $outFile;
    }

/* -----------------------------------------------------------------------------------------------------
* | OCR METHODS
* ----------------------------------------------------------------------------------------------------- */

    /**
     * Convert a single file to a searchable PDF.
     *
     * @param string $files The file path.
     * @param array $ret The array to store the resulting file paths.
     * @return array The array containing paths to the searchable PDF(s).
     */
    private function _toSearchableSingle($files, $ret = []){

        if($this->isSearchable($files)){
            $ret[] = $files;
            return $ret;
        }

        foreach($this->toImage($files, 'jpg', 400) as $image){
            $target         = $this->_randomName();
            $outputTarget   = str_replace('.pdf', '', $target);
            exec("export TESSDATA_PREFIX=~/conda/share/tessdata/ ; ~/conda/bin/tesseract {$image} {$outputTarget} pdf");
            $ret[]          = $target;
        }        

        return $ret;
    }
    
    /**
     * Convert one or more files to a searchable PDF.
     *
     * @param string|array $files The file path or an array of file paths.
     * @return string|array The path to the merged searchable PDF file or an array of paths to searchable PDF files.
     */
    public function toSearchablePdf($files){

        //CHECK IF MULTIPLE FILES WERE SENT
        if(is_array($files)){

            //INITIALIZE THE RETURN ARRAY
            $ret = [];

            //MAKE EACH FILE SEARCHABLE
            foreach($files as $file) $ret[] = $this->toSearchablePdf($file);

            //SEND BACK THE RETURN
            return $ret;
        }
       
        //GET THE PAGE COUNT
        $pageCount  = $this->pageCount($files);

        //INITIALIZE THE RETURN ARRAY
        $ret        = [];

        //IF THERE ARE MULTIPLE PAGES CONVERT EACH ONE TO SEARCHABLE
        if($pageCount > 1) foreach($this->split($files) as $tmpPage) $ret = $this->_toSearchableSingle($files, $ret);

        //IF THERE IS AT LEAST ONE PAGE CONVERT IT TO SEARCHABLE
        elseif($pageCount > 0) $ret = $this->_toSearchableSingle($files, $ret);

        //SEBD BACK THE MERGED PDF
        return $this->merge($ret);
    }

    /**
     * Extract text from a single PDF page.
     *
     * @param string $path The path to the PDF file.
     * @return string The extracted text.
     */
    private function _asTextSingle($path){

        if(checkMimeType($path)->extension() == 'pdf'){
            $output_path = $this->_randomName('txt');

            //EXTRACT THE TEXT TO PDF
            $pdfToTextPath = "~/conda/bin/pdftotext";
            //$pdfToTextPath = realpath(__DIR__.'/../binaries/pdftotext');
            exec("{$pdfToTextPath} -layout {$path} {$output_path}");

            //ADD THE TEXT CONTENT TO THE PAGES ARRAY
            $page = file_get_contents($output_path);

            $chr_map = array(
               // Windows codepage 1252
               "\xC2\x82" => "'", // U+0082⇒U+201A single low-9 quotation mark
               "\xC2\x84" => '"', // U+0084⇒U+201E double low-9 quotation mark
               "\xC2\x8B" => "'", // U+008B⇒U+2039 single left-pointing angle quotation mark
               "\xC2\x91" => "'", // U+0091⇒U+2018 left single quotation mark
               "\xC2\x92" => "'", // U+0092⇒U+2019 right single quotation mark
               "\xC2\x93" => '"', // U+0093⇒U+201C left double quotation mark
               "\xC2\x94" => '"', // U+0094⇒U+201D right double quotation mark
               "\xC2\x9B" => "'", // U+009B⇒U+203A single right-pointing angle quotation mark

               // Regular Unicode     // U+0022 quotation mark (")
                                      // U+0027 apostrophe     (')
               "\xC2\xAB"     => '"', // U+00AB left-pointing double angle quotation mark
               "\xC2\xBB"     => '"', // U+00BB right-pointing double angle quotation mark
               "\xE2\x80\x98" => "'", // U+2018 left single quotation mark
               "\xE2\x80\x99" => "'", // U+2019 right single quotation mark
               "\xE2\x80\x9A" => "'", // U+201A single low-9 quotation mark
               "\xE2\x80\x9B" => "'", // U+201B single high-reversed-9 quotation mark
               "\xE2\x80\x9C" => '"', // U+201C left double quotation mark
               "\xE2\x80\x9D" => '"', // U+201D right double quotation mark
               "\xE2\x80\x9E" => '"', // U+201E double low-9 quotation mark
               "\xE2\x80\x9F" => '"', // U+201F double high-reversed-9 quotation mark
               "\xE2\x80\xB9" => "'", // U+2039 single left-pointing angle quotation mark
               "\xE2\x80\xBA" => "'", // U+203A single right-pointing angle quotation mark
            );
            $chr = array_keys  ($chr_map); // but: for efficiency you should
            $rpl = array_values($chr_map); // pre-calculate these two arrays
            $page = str_replace($chr, $rpl, html_entity_decode($page, ENT_QUOTES, "UTF-8"));

            $page = str_replace(["\f"], [''], $page);

            return $page;
        }

        return '';        
    }

    /**
     * Extract text from a PDF.
     *
     * @param string|array $path The path to the PDF file or an array of PDF file paths.
     * @return \Illuminate\Support\Collection The extracted text.
     */
    public function asText($path){

        if(is_array($path)){
            $text = collect([]);
            foreach($path as $file) $text[] = $this->asText($file);
            return $text;
        }

        $text = collect([]);
        foreach($this->split($path) as $file) $text[] = $this->_asTextSingle($file);
        return $text;
    }

    /**
     * Check if the PDF is searchable.
     *
     * @param string $path The path to the PDF file.
     * @return bool True if the PDF is searchable, false otherwise.
     */
    public function isSearchable($path){
        return !empty(trim($this->_asTextSingle($path)));
    }
}
