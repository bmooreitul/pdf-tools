<?php

namespace Itul\PdfTools;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\PdfTools\Skeleton\SkeletonClass
 */
class PdfToolsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pdfTools';
    }
}
