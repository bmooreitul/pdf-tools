# PDF Tools

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/pdf-tools.svg?style=flat-square)](https://packagist.org/packages/itul/pdf-tools)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/pdf-tools.svg?style=flat-square)](https://packagist.org/packages/itul/pdf-tools)
![GitHub Actions](https://github.com/itul/pdf-tools/actions/workflows/main.yml/badge.svg)

A shorthand collection of methods commonly needed for pdf generation, manipulation and conversion.

## Requirements

Behind the scenes this package leverages [pdftotext](https://en.wikipedia.org/wiki/Pdftotext). You can verify if the binary installed on your system by issueing this command:

```bash
which pdftotext
```

If it is installed it will return the path to the binary.

To install the binary you can use this command on Ubuntu or Debian:

```bash
apt-get install poppler-utils
```

On a mac you can install the binary using brew

```bash
brew install poppler
```

If you're on RedHat, CentOS, Rocky Linux or Fedora use this:

```bash
yum install poppler-utils
```

## Installation

You can install the package via composer:

```bash
composer require itul/pdf-tools
```

## Usage

```php
// Usage description here
```

#### General Methods

##### init()
- **Return type:** static
- **Description:** Class method for initializing an instance of the class.

#### Modification Methods

##### split($path)
- **Parameters:**
  - $path (string): The path to the PDF file.
- **Return type:** array
- **Description:** Split a PDF into individual pages.

##### merge(array $paths)
- **Parameters:**
  - $paths (array): An array of paths to PDF files to merge.
- **Return type:** string
- **Description:** Merge multiple PDF files into a single PDF.

##### compress($path)
- **Parameters:**
  - $path (string|array): The path to the PDF file or an array of paths.
- **Return type:** string|array
- **Description:** Compress a PDF file.

##### uncompress($path)
- **Parameters:**
  - $path (string|array): The path to the compressed PDF file or an array of paths.
- **Return type:** string|array
- **Description:** Uncompress PDF data.

##### flatten($path)
- **Parameters:**
  - $path (string|array): The path to the PDF file or an array of paths.
- **Return type:** string|array
- **Description:** Flatten a PDF to make text not selectable.

##### stamp($path, string $stampPath, $allPages = false)
- **Parameters:**
  - $path (string|array): The path to the PDF file or an array of paths.
  - $stampPath (string): The path to the PDF file to use as a stamp.
  - $allPages (bool): Whether to stamp all pages of the PDF.
- **Return type:** string|array
- **Description:** Stamp a PDF with another PDF.

##### fill(string $path, array $data = [])
- **Parameters:**
  - $path (string): The path to the PDF file.
  - $data (array): An associative array containing form field data.
- **Return type:** string
- **Description:** Fill PDF form fields with data.

#### Data Methods

##### pdfInfo(string $path)
- **Parameters:**
  - $path (string): The path to the PDF file.
- **Return type:** \GeneralObject
- **Description:** Get PDF information.

##### pageCount(string $path)
- **Parameters:**
  - $path (string): The path to the PDF file.
- **Return type:** float
- **Description:** Get the number of pages in a PDF.


#### Conversion Methods

##### toPdf($path)
- **Parameters:**
  - $path (string|array): The path to the file or an array of file paths.
- **Return type:** string|array
- **Description:** Convert a file to a PDF.

##### toImage(string $path, $format = 'png', $density = 300)
- **Parameters:**
  - $path (string): The path to the file.
  - $format (string): The image format to convert to (default is 'png').
  - $density (int): The density for the image conversion (default is 300).
- **Return type:** array
- **Description:** Convert a file to an image.

#### HTML Rendering Methods

##### fromHTML(string $html)
- **Parameters:**
  - $html (string): The HTML content or file path.
- **Return type:** string
- **Description:** Convert an HTML file or a string of HTML to a PDF.

##### fromView($view, $data = [])
- **Parameters:**
  - $view (string): The name of the view.
  - $data (array): The data to pass to the view.
- **Return type:** string
- **Description:** Render a Laravel view as a PDF.

##### toHTML($path, $raster = false, $gen = true)
- **Parameters:**
  - $path (string): The path to the fillable PDF.
  - $raster (bool): Whether to rasterize the PDF.
  - $gen (bool): Whether to generate the parsed HTML.
- **Return type:** mixed
- **Description:** Convert a fillable PDF to an HTML form.

##### fillableFromHtml(string $pageHtml, string $pageStyle = null)
- **Parameters:**
  - $pageHtml (string): The HTML content or file path.
  - $pageStyle (string|null): The CSS style content or file path.
- **Return type:** string
- **Description:** Convert a single HTML page to a fillable PDF form.

#### OCR Methods

##### toSearchablePdf($files)
- **Parameters:**
  - $files (string|array): The file path or an array of file paths.
- **Return type:** string|array
- **Description:** Convert one or more files to a searchable PDF.

##### asText($path)
- **Parameters:**
  - $path (string|array): The path to the PDF file or an array of PDF file paths.
- **Return type:** \Illuminate\Support\Collection
- **Description:** Extract text from a PDF.

##### isSearchable($path)
- **Parameters:**
  - $path (string): The path to the PDF file.
- **Return type:** bool
- **Description:** Check if the PDF is searchable.

